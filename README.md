<!-- -*-Mode: markdown;-*- -->
<!-- $Id$ -->

MIAMI-NW
=============================================================================

**Home**: 
  - https://gitlab.com/perflab-exact/palm/miami-nw

  - [Performance Lab for EXtreme Computing and daTa](https://github.com/perflab-exact)


**About**: MIAMI-NW ("MIAMI-northwest") is an updated version of MIAMI
(https://github.com/gmarin13/MIAMI) that supports modern versions of
Intel Pin and contains bug fixes.
>  MIAMI (Machine Independent Application Models for performance
>  Insight) is a collection of tools for automatic construction of
>  application centric, thread-level, performance models, focused on
>  facilitating performance prediction and micro-architecture level
>  performance diagnosis. At its lowest level, MIAMI uses static and
>  dynamic analysis of fully optimized x86-64 application binaries.

MIAMI was designed for Pin 2. Unfortunately, Pin 2 is deprecated and
no longer runs on modern versions of Linux. For example, it does not
work on RHEL 7.x. In particular, Pin 2 requires a C++ ABI <= GCC 4.4.

MIAMI-NW includes support for Pin 3.x. This support required non-trivial
changes because Pin's requirements changed substantially between 2.x and
3.x. The changes are documented below.


**Contacts**: (_firstname_._lastname_@pnnl.gov)
  - Nathan R. Tallent ([www](https://hpc.pnnl.gov/people/tallent)), ([www](https://www.pnnl.gov/people/nathan-tallent))
  - Ozgur Kilic


Acknowledgements
-----------------------------------------------------------------------------

This work was supported by the U.S. Department of Energy's Office of
Advanced Scientific Computing Research:
- Integrated End-to-end Performance Prediction and Diagnosis

Original MIAMI READMEs and license are located in <README> directory.


Changes in MIAMI-NW
=============================================================================

- Support Pin 3.x, which required extensive changes.

  For MIAMI/cfgprof and MIAMI/memreuse, remove all uses of C++
  RTTI, C++ exceptions, and C++11.

  For MIAMI/miami(schedtool), remove its support on Pin (see reasons
  below). Replace its use of Pin for parsing options and loading
  binary images. Options are now parsed with argp; binary images are
  now loaded with GNU binutils.

- Provide legacy support for Pin 2.x (for validation). Remove general
  support for Pin 2.x as it only works with legacy C++ runtimes and
  compilers.

- Bug Fix: Index load modules by name rather than index. When reading
  a binary's load module dependences with Pin, the list was not always
  the same.  Thus, if MIAMI/memreuse recorded a load map in one order,
  the MIAMI/miami(schedtool) tool might read it in another order. The
  result is that accessing load modules by index couuld be wrong.


MIAMI (Legacy Pin 2.x)
=============================================================================

URL: https://github.com/gmarin13/MIAMI

Requirements:
- GCC <= 4.4 (Pin 2.14 requires C++ ABI <= GCC 4.4)
- Intel pin-2.13-62732-gcc.4.4.7-linux
- GNU binutils 2.23

Code:
  ```sh
  git clone https://github.com/gmarin13/MIAMI.git
  ```

MIAMI-NW is based on last commit as of writing:
  2873f09e6918424685af6e7125a1dd5c6616d929 (Jun 19 12:54:19 2014)


-----------------------------------------------------------------------------
Details of changes in MIAMI-NW
=============================================================================

- Support Pin 3.x (cfgprof, memreuse) as Pin 2.x only works with legacy
  systems, Xed, and compilers. 
  
    This required extensive changes.  First, it required removing use
    of C++ RTTI, C++ exceptions, and C++11.  Second, MIAMI/schedtool
    could no longer be a Pin tool (see below), which required replacing
    use of Pin's option parser with argp; and replacing use of Pin for
    loading binary with Binutils.

- Legacy support for Pin 2.x

- Bug Fix: Index DOS by name rather than index. When reading a
  binary's DSO dependences with Pin, the list was not always the same.
  Thus, if memreuse recorded a load map in one order, the miami tool
  might read it in another order. The result is that accessing load
  modules by index couuld be wrong.


----------------------------------------
Notes on Pin 3.x
----------------------------------------

- Pin 3.x requres that the Pin tool use (link with) the provided C and
  C++ runtime. The provided C++ runtime and STL do not fully support
  C++11. The provided C runtime does not provide common extras in GNU
  libc such as:
  - stat(2), getrusage(2), umask(2)
  - dcgettext(3)
  - stdout, stderr, `__errno_location`
  - `__xstat`, `_IO_putc`, `__strdup`
  - `popcount`, `__popcountdi2`, `__rawmemchr`

- Pin 3.x does not support C++ RTTI in the Pin tool; thus a Pin tool
  cannot use dynamic_cast (compiles with -fno-rtti).
  
- Pin 3.x prohibits the use of exceptions in the Pin tool

- Pin 3.x does not support full static linking as it no longer contains
  static versions of its libraries.

- Pin build rules: Users Guide, "Pin's makefile Infrastructure"

----------------------------------------
Notes on binutils (Test with 2.26, 2.27)
----------------------------------------

- Build binutils; only need 'bfd', 'libiberty'
  ```sh
  ./configure --prefix=$HOME/pkg/binutils-2.2x
  ```

- Copy {demangle.h, libiberty.h} to <prefix>/include
- Copy {libiberty.a} to <prefix>/lib


----------------------------------------
Changes from MIAMI (Oldest to newest)
----------------------------------------

* Cleanup binutils interface: remove files: To ensure consistent
  definitions, Binutils headers should be part of binutils
  installation.
  - `externals/include/demangle.h`
  - `externals/include/libiberty.h`

* Cleanup binutils interface: Support binutils 2.26 (and others)
  - `miami.config.sample`
  - `miami.config`
  - `Makefile`
  - `src/Makefile`
  - `src/make.rules`
  - `src/Scheduler/makefile.pin`

* Support PIN 2.14
  - `src/tools/pin_config`
  
* Correct non-conforming C++11: `const_iterator`
  - `src/common/general_formula.h `
  - `src/Scheduler/scope_implementation.h`
  - `src/common/instruction_class.h`
  - `src/OAUtils/BaseGraph.h`

* Correct non-conforming C++11: space between literal and variable
  - `src/common/slice_references.C`
  - `src/common/cache_sim.C`
  - `src/Scheduler/memory_reuse_histograms.C`
  - `src/Scheduler/load_module.C`
  - `src/Scheduler/CFG.C`
  - `src/Scheduler/routine.C`
  
* Minor cosmetic changes
  - `src/Scheduler/load_module.C`
  - `src/Scheduler/routine.C`
  - `src/common/instr_info.H`

* New/tweaked debug ouptut
  - `src/common/instruction_decoding.h`
  - `src/common/SystemSpecific/x86_xed_decoding.C`
  - `src/common/register_class.C`
  - `src/common/instr_info.C`
  
* New debug hook for attaching to MIAMI.
  - `src/Scheduler/schedtool.C`

* Add hooks for debugging the tool launch.
  - `src/tools/pin_config`
  - `src/tools/run_static.in`
  - `src/tools/run_tool.in`

* Rename file to reflect compilation dependencies.
  - `src/common/instruction_decoding.C` -> `src/common/instruction_decoding_pin.C`
  - `src/MemReuse/makefile.pin`
  - `src/CFGtool/makefile.pin`
  - `src/Scheduler/makefile.pin`

* Remove compiler warning for possibly uninitialized variable.
  - `src/common/xml.C`

* Remove explicit dynamic exception specifications (i.e., 'throw' on
  on function declarations) as they are now deprecated in C++.

  - `src/OAUtils/BaseGraph.C`
  - `src/OAUtils/BaseGraph.h`
  - `src/Scheduler/SchedDG.C`
  - `src/Scheduler/SchedDG.h`

* Pin 3.x support: Convert `dynamic_cast<>` to `static_cast<>` to
  avoid using RTTI (Pin 3 prohibits RTTI, adding compiler option
  -fno-rtti). In these cases, the `static_cast<>` should always be
  correct, as the `dynamic_cast<>` would have never returned NULL.
  - `src/OAUtils/DGraph.h`
  - `src/OAUtils/DGraph.C`
  - `src/common/prog_scope.C`
  - `src/common/PrivateCFG.h`
  - `src/CFGtool/CFG.h`
  - `src/MemReuse/CFG.h`
  - `src/Scheduler/CFG.h`
  - `src/Scheduler/PatternGraph.h`
  - `src/Scheduler/CFG.C`
  - `src/Scheduler/CycleSetDAG.C`
  - `src/Scheduler/CycleSetDAG.h`
  - `src/Scheduler/DGDominator.h`
  - `src/Scheduler/PatternGraph.C`
  - `src/Scheduler/SchedDG.C`
  - `src/Scheduler/SchedDG.h`
  - `src/Scheduler/routine.C`

* Pin 3.x support: Cannot use exceptions within a Pin tool; replace
  them with assert().
  - `src/OAUtils/BaseGraph.C`
  
* Pin 3.x support: Pin 3.x runtime libraries do not support
  stat(). Replace with access().
  - `src/common/file_utilities.C`
  - `src/common/source_file_mapping_binutils.C`

* Pin 3.x support: Pin 3.x C++ STL does not support
  set::erase(const_iterator)
  - `src/common/general_formula.h`

* Pin 3.x support: Update makefiles to build with either Pin 3.x or
  2.x. (The build rules for Pin 3.x are much stricter than with Pin
  2.x.) All tools except miami (schedtool) should now build with
  either Pin version.
  - `miami.config`
  - `src/make.rules`
  - `src/CFGtool/makefile.pin `
  - `src/CacheSim/makefile.pin`
  - `src/MemReuse/makefile.pin`
  - `src/StreamSim/makefile.pin`
  - `src/Scheduler/makefile.pin`

* Pin 3.x support for 'cfgprof'. With the updated Xed, 'cfgprof'
  encounters new instructions. Add preliminary to cope with these
  instructions. Note that we do not fully model their pipeline
  performance!
  	`XED_ICLASS_XADD_LOCK`
  	`XED_ICLASS_ADD_LOCK`
  	`XED_ICLASS_BTS_LOCK`
  	`XED_ICLASS_SUB_LOCK`
  	`XED_ICLASS_CMPXCHG_LOCK`
  	`XED_ICLASS_DEC_LOCK`
  	`XED_ICLASS_INC_LOCK`
  	`XED_ICLASS_OR_LOCK`

    `XED_ICLASS_REPE_CMPSB`
    `XED_ICLASS_REP_STOSB`
    `XED_ICLASS_REP_STOSD`
    `XED_ICLASS_REP_STOSQ`
    `XED_ICLASS_REP_STOSW`
    `XED_ICLASS_REPNE_SCASB`
    `XED_ICLASS_REP_MOVSQ`
    `XED_ICLASS_REP_MOVSB`
    `XED_ICLASS_REP_MOVSD`
    `XED_ICLASS_REP_MOVSW`

    `XED_ICLASS_VMOVDQA64`

    `XED_ICLASS_TZCNT`

  - `src/common/SystemSpecific/IB_x86_xed.C`


* Removed use of a few other GNU libc symbols (e.g., stat)

* To support Pin 3.x, MIAMI/schedtool is no longer a Pin tool.

  Why can MIAMI/schedtool not be a Pin tool? It will effectively be
  impossible to build a version of miami/schedtool that links with Pin
  3.x. First, it will be necessary to remove the use of exceptions in
  src/scheduler/SchedDG.C. (This is possible, e.g., with
  setjmp/longjmp.) More importantly, Pin 3.x effectively prohibits
  linking with binutils, because the latter requires several glibc
  symbols not supplied in Pin's C runtime.
  
  - Replace use of Pin's option parser with argp.
    (src/Scheduler/schedtool.C).
  - Replace use of Pin for loading binary (IMG_open()) with Binutils.
    See 'OKilic' comments.
  - MIAMI/schedtool builds without Pin compiler options/dependences.

  - TODO: Replace use of deprecated Pin routines (see compilation warnings).


    `src/common/source_file_mapping_binutils.C`
	`src/Scheduler/schedtool.C`
	`src/Scheduler/MiamiDriver.C`
	`src/Scheduler/load_module.C`
	`src/Scheduler/load_module.h`
	`src/Scheduler/routine.C`
	`src/Scheduler/DGBuilder.C`

