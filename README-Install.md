-*-Mode: markdown;-*-

$Id$

-----------------------------------------------------------------------------
Prerequisites
=============================================================================

Environment
- Most any GCC (GCC 4+)
- Pin 3.x (Palm externals)
- Xed (Palm externals, instead of Pin)
- binutils 2.27 (Palm externals)

Legacy Environment for Pin 2.x (only for validating)!
- GCC <= 4.4 (Pin 2.14 requires C++ ABI <= GCC 4.4)
- pin-2.14-71313-gcc.4.4.7-linux (Palm externals)
- binutils 2.27 (Palm externals)


-----------------------------------------------------------------------------
Building & Installing
=============================================================================

1. Build Palm Externals package:
   https://gitlab.pnnl.gov/perf-lab/palm/palm-externals

2. Within Palm Externals, extract Pin 3.x

3. MIAMI-NW analyzer:
   https://gitlab.pnnl.gov/perf-lab/palm/miami-nw
   
   ```sh
   git clone https://gitlab.pnnl.gov/perf-lab/palm/miami-nw.git
   cd <miami-nw>
   make
   ```

-----------------------------------------------------------------------------
Using
=============================================================================

1. Gather dynamic instruction paths: Use cfgprof to generate a a
   dynamic CFG (only actual edges instead all possible edges) with
   inter-procedural edges. Result: A .cfg file.
   ```sh
   <miami-install>/bin/cfgprof -help -- /bin/ls
   <miami-install>/bin/cfgprof -- <app>
   <miami-install>/bin/cfgprof -stats -statistic -o 1M-100-2 ~/Results_Miami_New/seqAndRandom.out 1000000 100 2
   ```

2. Optional: Capture memory memory reuse distances. Result: A .mrdt file.
   ```sh
   <miami-install>/bin/memreuse -help -- /bin/ls
   <miami-install>/bin/memreuse -- <app>
   <miami-install>/bin/memreuse -footprint -- <app>
   <miami-install>/bin/memreuse -debug 0 -footprint -statistic -profile -o tenThousand -- ~/Results_Miami_New/withG.out 10000 10000 10
   ```

3. Analyze loops at the binary level using MIAMI:
   ```sh
   <miami-install>/bin/miami -c <cfg-file> -m <miami-install>/share/machines/x86_SandyBridge_1.mdf
   <miami-install>/bin/miami -c <cfg-file> -mrd <mrd-file> -m <x86_SandyBridge_EP.mdf>
   ```

4. View results:
   ```sh
   java -jar <miami-install>/Viewer/hpcviewer.jar <xml-file>
   ```
