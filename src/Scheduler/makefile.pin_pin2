# -*-Mode: makefile;-*-

##
## PIN tools
##

##############################################################
#
# Here are some things you might want to configure
#
##############################################################

include ../../miami.config

##############################################################
#
# Tools - you may wish to add your tool name to TOOL_ROOTS
#
##############################################################

TOOL_ROOTS = miami
TOOL_VAR = vars_sched
TOOL_OBJ_DIR = scheduler
BINSCRIPT = $(MIAMI_HOME)/src/tools/run_static.in

PIN_SCHEDULER_OBJS = schedtool.o 

SCHEDULER_OBJS = \
  CFG.o routine.o load_module.o debug_scheduler.o \
  block_path.o PathInfo.o TimeAccount.o Clique.o FindCliques.o \
  reuse_group.o Instruction.o Machine.o MemoryHierarchyLevel.o PatternGraph.o \
  InstTemplate.o dependency_type.o schedule_time.o StringAssocTable.o \
  imix_histograms.o imix_clustering.o imix_width_clustering.o \
  TemplateExecutionUnit.o UnitRestriction.o BitSet.o BypassRule.o prog_scope.o \
  machdesc.tab.o machdesc.lex.o MiamiDriver.o memory_latency_histograms.o \
  path_id.o CycleSetDAG.o SchedDG.o DGBuilder.o stream_reuse_histograms.o \
  XML_output.o memory_reuse_histograms.o TemplateInstantiate.o

PIN_COMMON_OBJS = instruction_decoding_pin.o

COMMON_OBJS = \
  PrivateCFG.o printable_class.o private_routine.o Assertion.o BaseDominator.o \
  default_values.o xml.o code_scope.o report_time.o source_file_mapping_binutils.o \
  file_utilities.o debug_miami.o loadable_class.o miami_globals.o miami_utils.o  \
  private_load_module.o slice_references.o Dominator.o mark_back_edges.o uop_code_cache.o \
  base_slice.o static_memory_analysis.o register_class.o instr_info.o instr_bins.o \
  static_branch_analysis.o instruction_class.o canonical_ops.o \
  math_routines.o 

OA_OBJS = BaseGraph.o DGraph.o

TARJ_OBJS = TarjanIntervals.o MiamiRIFG.o UnionFindUniverse.o

SO_TOOLS = $(TOOL_ROOTS:%=$(EXEDIR)%$(PINTOOL_SUFFIX)) 
SA_TOOLS = $(TOOL_ROOTS:%=$(EXEDIR)%$(SATOOL_SUFFIX)) 
SA_WRAPPER = $(TOOL_ROOTS:%=$(BINDIR)%) 

OBJS = $(SCHEDULER_OBJS:%=$(OBJDIR)$(TOOL_OBJ_DIR)/%) \
 $(COMMON_OBJS:%=$(OBJDIR)common/%) $(PIN_COMMON_OBJS:%=$(OBJDIR)pinobj/%) \
 $(OA_OBJS:%=$(OBJDIR)oautils/%) $(TARJ_OBJS:%=$(OBJDIR)tarjans/%)

SOMAIN_OBJS = $(PIN_SCHEDULER_OBJS:%=$(OBJDIR)pinobj/%)
SAMAIN_OBJS = $(PIN_SCHEDULER_OBJS:%=$(OBJDIR)pinstatic/%)

OPT_TOOLS = $(TOOL_ROOTS:%=$(OBJDIR)%_opt$(PINTOOL_SUFFIX)) 
OPT_OBJS = $(OBJS:.o=.oo)

YACC = bison
FLEX = flex

TOOLS = $(SA_TOOLS) $(SA_WRAPPER)
MAIN_PIN_OBJS = $(SAMAIN_OBJS)

ifeq ($(COMPILE_MIAMI_SO),yes)
   TOOLS += $(SO_TOOLS)
   MAIN_PIN_OBJS += $(SOMAIN_OBJS)
endif

# finds files for the compiling rules
vpath %.C $(MIAMI_HOME)/src/Scheduler
vpath %.C $(MIAMI_HOME)/src/common
vpath %.C $(MIAMI_HOME)/src/OAUtils
vpath %.C $(MIAMI_HOME)/src/tarjans

##############################################################
#
# build rules
#
##############################################################
#OPT = -O0
include ../make.rules

.force:

$(OBJDIR): .force
	mkdir -p $(OBJDIR)
	mkdir -p $(OBJDIR)pinobj
	mkdir -p $(OBJDIR)pinstatic
	mkdir -p $(OBJDIR)$(TOOL_OBJ_DIR)
	mkdir -p $(OBJDIR)common
	mkdir -p $(OBJDIR)oautils
	mkdir -p $(OBJDIR)tarjans

$(OBJDIR)$(TOOL_OBJ_DIR)/machdesc.tab.o : machdesc.tab.c machdesc.tab.h
	$(CXX) $(TOOL_CXXFLAGS) $(CXXFLAGS) -c -o $@ $<

$(OBJDIR)$(TOOL_OBJ_DIR)/machdesc.tab.oo : machdesc.tab.c machdesc.tab.h
	$(CXX) $(TOOL_CXXFLAGS) $(OPT_CXXFLAGS) -c -o $@ $<

$(OBJDIR)$(TOOL_OBJ_DIR)/machdesc.lex.o : machdesc.lex.c machdesc.tab.h
	$(CXX) $(TOOL_CXXFLAGS) $(CXXFLAGS) -c -o $@ $<

$(OBJDIR)$(TOOL_OBJ_DIR)/machdesc.lex.oo : machdesc.lex.c machdesc.tab.h
	$(CXX) $(TOOL_CXXFLAGS) $(OPT_CXXFLAGS) -c -o $@ $<

machdesc.tab.c: machine_description.y
	$(YACC) -d -b machdesc machine_description.y

machdesc.tab.h: machine_description.y
	$(YACC) -d -b machdesc machine_description.y

machdesc.lex.c: machine_description.l
	$(FLEX) -t machine_description.l > machdesc.lex.c

$(TOOLS): $(OBJS) $(MAIN_PIN_OBJS)

# Used when COMPILE_MIAMI_SO = yes
$(SO_TOOLS): $(OBJS) $(SOMAIN_OBJS)
	$(LINKER) -v $(DBG) $(TOOL_LDFLAGS) $(LINK_EXE)$@ \
	  $(SOMAIN_OBJS) \
	  $(OBJS) \
	  $(BINUTILS_LDFLAGS) \
	  $(TOOL_LPATHS) $(TOOL_LIBS)

$(SA_TOOLS) : $(OBJS) $(SAMAIN_OBJS)
	$(LINKER) -v $(DBG) $(SATOOL_LDFLAGS) $(LINK_EXE)$@ \
	  $(SAMAIN_OBJS) \
	  $(OBJS) \
	  $(SATOOL_LPATHS) $(SATOOL_LIBS) \
	  $(BINUTILS_LDFLAGS)

# tallent
#$(OBJDIR)$(TOOL_OBJ_DIR)/SchedDG.o : SchedDG.C
#	$(CXX) $(TOOL_CXXFLAGS) -fexceptions $(CXXFLAGS) -c -o $@ $<

# V4: Links for 2.14/3.x, but SEGV
#   Same as V3 below, but avoid SA* versions of PIN variables,
#   i.e., SATOOL_LDFLAGS, SATOOL_LPATHS, SATOOL_LIBS. (With Pin 3.x,
#   static linking is deprecated)
#	$(LINKER) -v $(DBG) $(TOOL_LDFLAGS) $(LINK_EXE)$@ \
#	  $(SAMAIN_OBJS) $(TOOL_LPATHS) $(TOOL_LIBS) \
#	  $(OBJS) \
#	  $(BINUTILS_LDFLAGS) \
#	  -lxed -lm

# V3: Good for 2.14; Bad for 3.x: Reorder so that Pin objs/libs come first
#	$(LINKER) -v $(DBG) $(SATOOL_LDFLAGS) $(LINK_EXE)$@ \
#	  $(SAMAIN_OBJS) $(SATOOL_LPATHS) $(SATOOL_LIBS) \
#	  $(OBJS) \
#	  $(BINUTILS_LDFLAGS) \
#	  -lxed -lm

# V2: Good for 2.14; Bad ln for 3.x: Same as V1, but avoid special linking flags
#	$(LINKER) -v $(DBG) $(SATOOL_LDFLAGS) $(LINK_EXE)$@ \
#	  $(OBJS) \
#	  $(BINUTILS_LDFLAGS) \
#	  $(SAMAIN_OBJS) \
#	  $(SATOOL_LPATHS) $(SATOOL_LIBS) -lm

# V1: Good for 2.14; Bad ln for 3.x: Original order
#	$(LINKER) -v $(DBG) $(SATOOL_LDFLAGS) $(LINK_EXE)$@ \
#	  $(OBJS) \
#	  -Wl,-Bstatic $(BINUTILS_LDFLAGS) \
#	  $(SAMAIN_OBJS) \
#	  -Wl,--as-needed -Wl,-Bdynamic -lm $(SATOOL_LPATHS) $(SATOOL_LIBS)


$(SA_WRAPPER) : $(SA_TOOLS)
	@sed -e 's/XTOOLVARX/$(TOOL_VAR)/g' -e 's/XTOOLLIBX/$(TOOL_ROOTS)$(SATOOL_SUFFIX)/g' $(BINSCRIPT) > $(BINDIR)$(TOOL_ROOTS)
	@chmod +x $(BINDIR)$(TOOL_ROOTS)

$(OPT_TOOLS): %$(PINTOOL_SUFFIX) : $(OPT_OBJS)
	$(LINKER) -v $(TOOL_LDFLAGS) $(LINK_EXE)$@ \
	$(OPT_OBJS) \
	$(TOOL_LPATHS) $(TOOL_LIBS) $(DBG) \
	$(BINUTILS_LDFLAGS)
